duniter-parameters-parser convertit le format brut des paramètres du bloc 0 d'une monnaie basée sur Duniter en un objet où chaque paramètre est associé à ce à quoi il correspond.
[Référentiel de conversion](https://git.duniter.org/nodes/typescript/duniter/blob/1.6/doc/Protocol.md#protocol-parameters)

## Usage:
```javascript
const parser = require("duniter-parameters-parser");
const rawParameters = "<paramètres issus du bloc 0>";
const parameters = parser(rawParameters);
```
