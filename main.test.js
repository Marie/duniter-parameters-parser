const data = [ '0.0488','86400','1000','432000','100','5259600','63115200',
  '5','5259601','5259602','0.8','31557600','6','24','300','12','0.67',
  '1488970800','1490094000','15778800' ];
const app = require("./main");
const expected = {
  c:0.0488, dt:86400, ud0:1000, sigPeriod:432000, sigStock:100,
  sigWindow:5259600, sigValidity:63115200, sigQty:5, idtyWindow:5259601,
  msWindow:5259602, xpercent:0.8, msValidity:31557600, stepMax:6,
  medianTimeBlocks:24, avgGenTime:300, dtDiffEval:12, percentRot:0.67,
  dtReeval:15778800, udTime0:1488970800, udReevalTime0:1490094000
}
describe('app convert raw data to expected', () => {
  const resultat = app(data);
  for(let key in expected){
    it(`app parse ${key}`, () => {
      expect(resultat[key]).toBe(expected[key]);
    });
  }
});
describe('app returns Ǧ1 parameters by default', () => {

  const G1default = {
    c:0.0488, dt:86400, ud0:1000, sigPeriod:432000, sigStock:100,
    sigWindow:5259600, sigValidity:63115200, sigQty:5, idtyWindow:5259600,
    msWindow:5259600, xpercent:0.8, msValidity:31557600, stepMax:5,
    medianTimeBlocks:24, avgGenTime:300, dtDiffEval:12, percentRot:0.67,
    udTime0:1488970800, udReevalTime0:1490094000, dtReeval:15778800
  };
  const resultat = app();
  for(let key in G1default){
    it(`app default ${key}`, () => {
      expect(resultat[key]).toBe(G1default[key]);
    });
  }
});
describe('app expose doc', () => {
  it(`doc.en.c has value`, () => {
    expect(app().doc.en.c).toBeDefined();//("The %growth of the UD every [dt] period");
  });
  it(`doc.en also exist with longName`, () => {
    expect(app().doc.en.growthSpeed).toBeDefined();//("The %growth of the UD every [dt] period");
  });

});
