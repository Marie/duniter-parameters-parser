const parametersOrder = [
  "c",
  "dt",
  "ud0",
  "sigPeriod",
//  "msPeriod",
  "sigStock",
  "sigWindow",
  "sigValidity",
  "sigQty",
  "idtyWindow",
  "msWindow",
  "xpercent",
  "msValidity",
  "stepMax",
  "medianTimeBlocks",
  "avgGenTime",
  "dtDiffEval",
  "percentRot",
  "udTime0",
  "udReevalTime0",
  "dtReeval"
//  ,"txWindow"
];

module.exports = function(data=[]){
  const resultat = {
    c:0.0488, dt:86400, ud0:1000, sigPeriod:432000, sigStock:100,
    sigWindow:5259600, sigValidity:63115200, sigQty:5, idtyWindow:5259600,
    msWindow:5259600, xpercent:0.8, msValidity:31557600, stepMax:5,
    medianTimeBlocks:24, avgGenTime:300, dtDiffEval:12, percentRot:0.67,
    udTime0:1488970800, udReevalTime0:1490094000, dtReeval:15778800
  };
  for(let i in data) {
    const parameter = parametersOrder[i];
    resultat[parameter] = parseFloat(data[i]);
  }
  resultat.doc = {
    en:{
      c:"The %growth of the UD every [dtReeval] period",
      growthSpeed:"The %growth of the UD every [udReevaluationInterval] period",
      dt:"Time period between two UD.",
      udInterval:"Time period between two UD.",
      dtReeval:"Time period between two re-evaluations of the UD.",
      udReevaluationInterval:"Time period between two re-evaluations of the UD.",
      ud0:"UD(0), i.e. initial Universal Dividend",
      ud0Amount:"UD(0), i.e. initial Universal Dividend",
      udTime0:"Timestamp of first UD.",
      ud0Time:"Timestamp of first UD.",
      udReevalTime0:"Timestamp of first reevaluation of the UD.",
      udReevaluationFirstTime:"Timestamp of first reevaluation of the UD.",
      sigPeriod:"Minimum delay between 2 certifications of a same issuer, in seconds. Must be positive or zero.",
      certifMinInterval:"Minimum delay between 2 certifications of a same issuer, in seconds. Must be positive or zero.",
      msPeriod:"Minimum delay between 2 memberships of a same issuer, in seconds. Must be positive or zero.",
      membershipAttemptMinInterval:"Minimum delay between 2 memberships of a same issuer, in seconds. Must be positive or zero.",
      sigStock:"Maximum quantity of active certifications made by member.",
      maxActiveCertif:"Maximum quantity of active certifications made by member.",
      sigWindow:"Maximum delay a certification can wait before being expired for non-writing.",
      pendingCertifLifetime:"Maximum delay a certification can wait before being expired for non-writing.",
      sigValidity:"Maximum age of an active signature (in seconds)",
      certifLifetime:"Maximum age of an active signature (in seconds)",
      sigQty:"Minimum quantity of signatures to be part of the WoT",
      membershipRequirements_minCertifQty:"Minimum quantity of signatures to be part of the WoT",
      idtyWindow:"Maximum delay an identity can wait before being expired for non-writing.",
      pendingIdentityLifetime:"Maximum delay an identity can wait before being expired for non-writing.",
      msWindow:"Maximum delay a membership can wait before being expired for non-writing.",
      pendingMembershipLifetime:"Maximum delay a membership can wait before being expired for non-writing.",
      xpercent:"Minimum percent of sentries to reach to match the distance rule",
      membershipRequirements_reachableFromMinPercentSentry:"Minimum percent of sentries to reach to match the distance rule",
      msValidity:"Maximum age of an active membership (in seconds)",
      membershipLifetime:"Maximum age of an active membership (in seconds)",
      stepMax:"Maximum distance between each WoT member and a newcomer",
      membershipRequirements_maxStepFromSentry:"Maximum distance between each WoT member and a newcomer",
      medianTimeBlocks:"Number of blocks used for calculating median time.",
      blocksRangeToComputeMedianTime:"Number of blocks used for calculating median time.",
      avgGenTime:"The average time for writing 1 block (wished time)",
      blockComputationAvgTime:"The average time for writing 1 block (wished time)",
      dtDiffEval:"The number of blocks required to evaluate again PoWMin value",
      blocksRangeForBlockComputationDifficultyCalculation:"The number of blocks required to evaluate again PoWMin value",
      percentRot:"The percent of previous issuers to reach for personalized difficulty",
      blockComputationCustomDifficultyMagic:"The percent of previous issuers to reach for personalized difficulty",
      txWindow:"= 3600 * 24 * 7. Maximum delay a transaction can wait before being expired for non-writing.",
      pendingTransactionLifetime:"= 3600 * 24 * 7. Maximum delay a transaction can wait before being expired for non-writing."
    },
    fr:{}
  }
  //TODO: doc.fr et doc.en
  //TODO: humanReadable
  //TODO: longName.fr .en
  return resultat;
}
